Miscellaneous objects
=====================

.. autoclass:: gpaw.lfc.NewLocalizedFunctionsCollection
    :members:

.. autoclass:: gpaw.spline.Spline
    :members:

.. autoclass:: gpaw.poisson.FDPoissonSolver
    :members:

.. autoclass:: gpaw.xc.functional.XCFunctional
    :members:

.. autoclass:: gpaw.xc.gga.GGA
    :members:

.. autoclass:: gpaw.forces.ForceCalculator
    :members:

.. autoclass:: gpaw.grid_descriptor.GridDescriptor
    :members:

.. autoclass:: gpaw.scf.SCFLoop
    :members:
